package main

import (
	"flag"
	"io/ioutil"
	"log"

	"git.autistici.org/ai3/go-common/serverutil"
	"gopkg.in/yaml.v3"

	"git.autistici.org/id/usermetadb/server"
)

var (
	addr       = flag.String("addr", ":5005", "address to listen on")
	configFile = flag.String("config", "/etc/user-meta-server.yml", "path of config file")
)

type Config struct {
	server.Config `yaml:",inline"`
	ServerConfig  *serverutil.ServerConfig `yaml:"http_server"`
}

// Read the YAML configuration file.
func loadConfig() (*Config, error) {
	data, err := ioutil.ReadFile(*configFile)
	if err != nil {
		return nil, err
	}
	// Set some default values.
	var config Config
	config.CompactionKeepCount = 100
	config.MaxAgeDays = 365
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, err
	}
	return &config, nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	config, err := loadConfig()
	if err != nil {
		log.Fatal(err)
	}

	db, err := server.New(&config.Config)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	log.Printf("starting user-meta-server on %s", *addr)
	if err := serverutil.Serve(db.Handler(), config.ServerConfig, *addr); err != nil {
		log.Fatal(err)
	}
}
