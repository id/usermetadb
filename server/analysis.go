package server

import (
	"context"
	"database/sql"

	"git.autistici.org/ai3/go-common/sqlutil"
	"git.autistici.org/id/usermetadb"
)

var analysisStatements = map[string]string{
	"check_device_info": `SELECT 1 FROM devices WHERE username = ? AND id = ?`,
}

type analysisService struct {
	db *sql.DB
}

func newAnalysisService(db *sql.DB) (*analysisService, error) {
	return &analysisService{
		db: db,
	}, nil
}

func (d *analysisService) Close() {
}

func (d *analysisService) CheckDevice(ctx context.Context, username string, deviceInfo *usermetadb.DeviceInfo) (bool, error) {
	var seen bool
	err := sqlutil.WithReadonlyTx(ctx, d.db, func(tx *sql.Tx) error {
		err := tx.QueryRow(analysisStatements["check_device_info"], username, deviceInfo.ID).Scan(&seen)
		if err != nil && err != sql.ErrNoRows {
			return err
		}
		return nil
	})
	return seen, err
}
