package server

import (
	"database/sql"

	"git.autistici.org/ai3/go-common/sqlutil"
)

var migrations = []func(*sql.Tx) error{
	sqlutil.Statement(`
-- We store the raw denormalized user logs, because device information
-- might change over time (think a version update, or restoring a
-- backup to a different OS) and it may be useful to see the
-- historical variation. Unique device information is also aggregated
-- incrementally to a separate table, to provide a quick way to obtain
-- the list of devices for a user without a large table scan.
CREATE TABLE devices (
       id VARCHAR(64) NOT NULL,
       username TEXT NOT NULL,
       device_browser TEXT,
       device_os TEXT,
       device_mobile BOOL,
       first_seen DATETIME,
       last_seen DATETIME,
       last_device_remote_zone TEXT,
       last_device_user_agent TEXT
);`, `
CREATE UNIQUE INDEX idx_devices_id_username ON devices (id, username);
`, `
CREATE TABLE userlog (
       username TEXT NOT NULL,
       service TEXT NOT NULL,
       log_type TEXT NOT NULL,
       login_method TEXT,
       message TEXT,
       device_id VARCHAR(64) NOT NULL,
       device_remote_zone TEXT,
       device_user_agent TEXT,
       device_browser TEXT,
       device_os TEXT,
       device_mobile BOOL,
       timestamp DATETIME
);`, `
CREATE INDEX idx_userlog_username ON userlog (username);
`, `
CREATE INDEX idx_userlog_device_id ON userlog (device_id);
`),

	sqlutil.Statement(`
CREATE TABLE lastlogin (
	username TEXT NOT NULL,
	service TEXT NOT NULL,
	timestamp DATETIME NOT NULL
);
`, `
CREATE UNIQUE INDEX idx_lastlogin_username_service ON lastlogin (username, service);
`, `
CREATE INDEX idx_lastlogin_username ON lastlogin (username);
`),

	sqlutil.Statement(`
PRAGMA foreign_keys=off;
`, `
ALTER TABLE userlog RENAME TO _userlog_old;
`, `
CREATE TABLE userlog (
       username TEXT NOT NULL,
       service TEXT NOT NULL,
       log_type TEXT NOT NULL,
       login_method TEXT,
       message TEXT,
       device_id VARCHAR(64),
       device_remote_zone TEXT,
       device_user_agent TEXT,
       device_browser TEXT,
       device_os TEXT,
       device_mobile BOOL,
       timestamp DATETIME
);
`, `
INSERT INTO userlog (username, service, log_type, login_method, message, device_id, device_remote_zone, device_user_agent, device_browser, device_os, device_mobile, timestamp)
 SELECT username, service, log_type, login_method, message, device_id, device_remote_zone, device_user_agent, device_browser, device_os, device_mobile, timestamp
  FROM _userlog_old;
`, `
DROP TABLE _userlog_old;
`, `
PRAGMA foreign_keys=on;
`),
	sqlutil.Statement(`
ALTER TABLE userlog ADD COLUMN login_authenticator_id TEXT;
`),
}

func openDB(dburi string) (*sql.DB, error) {
	return sqlutil.OpenDB(dburi, sqlutil.WithMigrations(migrations))
}
