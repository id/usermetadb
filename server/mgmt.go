package server

import (
	"database/sql"
	"sync"
	"time"
)

// Call a function at semi-regular intervals.
type cronJob struct {
	f func()
	d time.Duration

	stopCh chan struct{}
	doneCh chan struct{}
}

func newCron(f func(), d time.Duration) *cronJob {
	j := &cronJob{
		f:      f,
		d:      d,
		stopCh: make(chan struct{}),
		doneCh: make(chan struct{}),
	}
	go j.run()
	return j
}

// Stop the job and wait for it to exit.
func (j *cronJob) Stop() {
	close(j.stopCh)
	<-j.doneCh
}

// Wait some time, or be interrupted by Stop().
func (j *cronJob) waitSomeTime() bool {
	t := time.NewTimer(j.d)
	defer t.Stop()
	select {
	case <-j.stopCh:
		return false
	case <-t.C:
		return true
	}
}

func (j *cronJob) run() {
	defer close(j.doneCh)
	for {
		if !j.waitSomeTime() {
			return
		}

		j.f()
	}
}

type usernameSet struct {
	mx        sync.Mutex
	usernames map[string]struct{}
}

func newUsernameSet() *usernameSet {
	return &usernameSet{
		usernames: make(map[string]struct{}),
	}
}

func (s *usernameSet) add(username string) {
	s.mx.Lock()
	s.usernames[username] = struct{}{}
	s.mx.Unlock()
}

func (s *usernameSet) foreach(f func(string)) {
	s.mx.Lock()
	for username := range s.usernames {
		delete(s.usernames, username)
		f(username)
	}
	s.mx.Unlock()
}

// Leisurely consolidate logs for users, but only when necessary, and
// proceeding at a slow pace. We wake up once per minute, and clean up
// the log history for users that have logged in since.
func compactLogs(db *sql.DB, username string, count int) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	if _, err := tx.Exec(userlogDBStatements["consolidate_userlog"], username, count); err != nil {
		return err
	}
	return tx.Commit()
}

func runPruneStmt(db *sql.DB, stmtName string, cutoff time.Time) error {
	// Run each batch deletion in its own transaction, just to be nice.
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	_, err = tx.Exec(userlogDBStatements[stmtName], cutoff)
	if err != nil {
		tx.Rollback() // nolint
		return err
	}
	return tx.Commit()
}

// Remove old entries (both logs and devices) from the database.
func pruneLogs(db *sql.DB, pruneCutoffDays int) (rerr error) {
	cutoff := time.Now().AddDate(0, 0, -pruneCutoffDays)

	// Always run both statements, even if the first returns an error.
	for _, stmtName := range []string{"prune_userlog", "prune_device_info"} {
		if err := runPruneStmt(db, stmtName, cutoff); err != nil {
			rerr = err
		}
	}

	// Trigger incremental auto-vacuum.
	//
	// nolint: errcheck
	db.Exec("PRAGMA main.incremental_vacuum")

	return
}
