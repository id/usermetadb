package server

import (
	"context"
	crand "crypto/rand"
	"database/sql"
	"encoding/hex"
	"math/rand"
	"os"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"git.autistici.org/id/usermetadb"
)

var (
	randomUsernames  = []string{"user1", "user2", "user3", "user4", "user5"}
	randomServices   = []string{"mail", "panel"}
	randomBrowsers   = []string{"Firefox", "Chrome", "IE", "Safari"}
	randomOSes       = []string{"Linux", "Windows", "iOS", "Android", "OSX"}
	randomUserAgents = []string{
		"Mozilla/5.0 (Linux; Android 7.0; SM-J327VPP Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36",
		"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36",
		"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1",
		"Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko",
		"MobileSafari/604.1 CFNetwork/889.9 Darwin/17.2.0",
	}
)

func choose(l []string) string {
	return l[rand.Intn(len(l))]
}

func generateRandomDeviceInfo() *usermetadb.DeviceInfo {
	var buf [8]byte
	crand.Read(buf[:]) // nolint
	id := hex.EncodeToString(buf[:])
	return &usermetadb.DeviceInfo{
		ID:        id,
		Browser:   choose(randomBrowsers),
		UserAgent: choose(randomUserAgents),
		OS:        choose(randomOSes),
		Mobile:    false,
	}
}

func generateAllRandomDevices() map[string][]*usermetadb.DeviceInfo {
	byUser := make(map[string][]*usermetadb.DeviceInfo)
	for _, u := range randomUsernames {
		for i := 0; i < 3; i++ {
			byUser[u] = append(byUser[u], generateRandomDeviceInfo())
		}
	}
	return byUser
}

// Distribute times randomly over two years.
const twoYearsSeconds = 86400 * 365 * 2

func randomTime() time.Time {
	return time.Now().Add(-time.Duration(rand.Intn(twoYearsSeconds)) * time.Second)
}

func generateTestLogs(n int, userDevices map[string][]*usermetadb.DeviceInfo) []*usermetadb.LogEntry {
	out := make([]*usermetadb.LogEntry, 0, n)
	for i := 0; i < n; i++ {
		u := choose(randomUsernames)
		devs := userDevices[u]
		dev := devs[rand.Intn(len(devs))]
		e := &usermetadb.LogEntry{
			Username:    u,
			Service:     choose(randomServices),
			Type:        usermetadb.LogTypeLogin,
			LoginMethod: "password",
			Message:     "logged in",
			Timestamp:   randomTime().UTC(),
			DeviceInfo:  dev,
		}
		out = append(out, e)
	}
	return out
}

func bulkLoadTestLogs(t testing.TB, db *sql.DB) *usermetadb.LogEntry {
	tx, err := db.Begin()
	if err != nil {
		t.Fatalf("tx.Begin: %v", err)
	}

	insertLogStmt, err := tx.Prepare(userlogDBStatements["insert_userlog_with_device_info"])
	if err != nil {
		t.Fatalf("insert_userlog: %v", err)
	}
	defer insertLogStmt.Close()
	insertDeviceInfoStmt, err := tx.Prepare(userlogDBStatements["insert_device_info"])
	if err != nil {
		t.Fatalf("insert_device_info: %v", err)
	}
	defer insertDeviceInfoStmt.Close()

	userDevices := generateAllRandomDevices()
	entries := generateTestLogs(1000, userDevices)

	now := time.Now()
	for username, devs := range userDevices {
		for _, dev := range devs {
			// nolint
			insertDeviceInfoStmt.Exec(
				username,
				dev.ID,
				dev.RemoteZone,
				dev.UserAgent,
				dev.Browser,
				dev.OS,
				dev.Mobile,
				now,
				now,
			)
		}
	}

	for _, entry := range entries {
		if _, err := insertLogStmt.Exec(
			entry.Username,
			entry.Service,
			entry.Type,
			entry.LoginMethod,
			entry.LoginAuthenticatorID,
			entry.Message,
			entry.Timestamp,
			entry.DeviceInfo.ID,
			entry.DeviceInfo.RemoteZone,
			entry.DeviceInfo.UserAgent,
			entry.DeviceInfo.Browser,
			entry.DeviceInfo.OS,
			entry.DeviceInfo.Mobile,
		); err != nil {
			t.Fatalf("error in insert: %v", err)
		}
	}

	if err := tx.Commit(); err != nil {
		t.Fatalf("could not load test data: %v", err)
	}

	return entries[0]
}

func TestUserlog_AddLog(t *testing.T) {
	defer os.Remove("test.db")
	db, err := openDB("test.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	ulog, err := newUserlogDB(db, 0, 0, nil)
	if err != nil {
		t.Fatal(err)
	}
	defer ulog.Close()

	entries := generateTestLogs(100, generateAllRandomDevices())

	for _, e := range entries {
		if err := ulog.AddLog(context.TODO(), e); err != nil {
			t.Fatalf("AddLog(%+v): %v", e, err)
		}
	}
}

func TestUserlog_AddLog_NoDeviceInfo(t *testing.T) {
	defer os.Remove("test.db")
	db, err := openDB("test.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	ulog, err := newUserlogDB(db, 0, 0, nil)
	if err != nil {
		t.Fatal(err)
	}
	defer ulog.Close()

	// Create a LogEntry with a nil DeviceInfo.
	u := choose(randomUsernames)
	e := &usermetadb.LogEntry{
		Username:    u,
		Service:     choose(randomServices),
		Type:        usermetadb.LogTypeLogin,
		LoginMethod: "password",
		Message:     "logged in",
		Timestamp:   time.Now().UTC(),
	}

	if err := ulog.AddLog(context.TODO(), e); err != nil {
		t.Fatalf("AddLog(%+v): %v", e, err)
	}

	// Fetch back the log.
	entries, err := ulog.GetUserLogs(context.TODO(), u, 0, 0)
	if err != nil {
		t.Fatal("GetUserLogs()", err)
	}
	if len(entries) == 0 {
		t.Fatal("GetUserLogs() returned nil")
	}
}

func TestUserlog_AddLog_Concurrent(t *testing.T) {
	defer os.Remove("test.db")
	db, err := openDB("test.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	ulog, err := newUserlogDB(db, 0, 0, nil)
	if err != nil {
		t.Fatal(err)
	}
	defer ulog.Close()

	numGoroutines := 100
	numEntries := 10

	var wg sync.WaitGroup
	var errors uint32
	start := make(chan bool)
	for i := 0; i < numGoroutines; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			entries := generateTestLogs(numEntries, generateAllRandomDevices())
			<-start

			for _, e := range entries {
				if err := ulog.AddLog(context.TODO(), e); err != nil {
					atomic.AddUint32(&errors, 1)
				}
			}
		}()
	}

	close(start)
	wg.Wait()

	if errors > 0 {
		t.Fatalf("there have been %d errors", errors)
	}
}

func TestUserlog_GetUserDevices(t *testing.T) {
	defer os.Remove("test.db")
	db, err := openDB("test.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	bulkLoadTestLogs(t, db)

	ulog, err := newUserlogDB(db, 0, 0, nil)
	if err != nil {
		t.Fatal(err)
	}
	defer ulog.Close()

	// Existing user.
	devs, err := ulog.GetUserDevices(context.TODO(), "user1")
	if err != nil {
		t.Fatal("GetUserDevices(user1)", err)
	}
	if len(devs) == 0 {
		t.Fatal("GetUserDevices(user1) returned nil")
	}

	// Non-existing user.
	devs, err = ulog.GetUserDevices(context.TODO(), "nonexisting")
	if err != nil {
		t.Fatal("GetUserDevices(nonexisting)", err)
	}
	if len(devs) != 0 {
		t.Fatal("GetUserDevices(nonexisting) returned non-nil")
	}
}

func TestUserlog_GetUserLogs(t *testing.T) {
	defer os.Remove("test.db")
	db, err := openDB("test.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	bulkLoadTestLogs(t, db)

	ulog, err := newUserlogDB(db, 0, 0, nil)
	if err != nil {
		t.Fatal(err)
	}
	defer ulog.Close()

	// Existing user.
	entries, err := ulog.GetUserLogs(context.TODO(), "user1", 0, 0)
	if err != nil {
		t.Fatal("GetUserLogs(user1)", err)
	}
	if len(entries) == 0 {
		t.Fatal("GetUserLogs(user1) returned nil")
	}
	// All log entries should have type set to
	// LogTypeLogin. Verify this as a way to validate
	// deserialization.
	for _, e := range entries {
		if e.Type != usermetadb.LogTypeLogin {
			t.Errorf("entry %+v has bad type", e)
		}
	}

	// Non-existing user.
	entries, err = ulog.GetUserLogs(context.TODO(), "nonexisting", 0, 0)
	if err != nil {
		t.Fatal("GetUserLogs(nonexisting)", err)
	}
	if len(entries) != 0 {
		t.Fatal("GetUserLogs(nonexisting) returned non-nil")
	}
}

func TestUserLog_HasCronjobs(t *testing.T) {
	defer os.Remove("test.db")
	db, err := openDB("test.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	bulkLoadTestLogs(t, db)

	ulog, err := newUserlogDB(db, 100, 365, nil)
	if err != nil {
		t.Fatal(err)
	}
	defer ulog.Close()

	if nc := len(ulog.cronjobs); nc != 2 {
		t.Fatalf("unexpected number of cron jobs: got %d, exp %d", nc, 2)
	}
}
