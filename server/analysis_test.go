package server

import (
	"context"
	"os"
	"testing"
)

func TestAnalysis_CheckDevice(t *testing.T) {
	defer os.Remove("test.db")
	db, err := openDB("test.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	e := bulkLoadTestLogs(t, db)

	svc, err := newAnalysisService(db)
	if err != nil {
		t.Fatal(err)
	}
	defer svc.Close()

	// Check if we've seen a known device.
	seen, err := svc.CheckDevice(context.TODO(), e.Username, e.DeviceInfo)
	if err != nil {
		t.Fatal("CheckDevice():", err)
	}
	if !seen {
		t.Fatal("CheckDevice returned seen=false, expected true")
	}

	// Check an unknown device.
	seen, err = svc.CheckDevice(context.TODO(), "unknown_user", e.DeviceInfo)
	if err != nil {
		t.Fatal("CheckDevice():", err)
	}
	if seen {
		t.Fatal("CheckDevice returned seen=true, expected false")
	}
}
