package server

import (
	"context"
	"net/http/httptest"
	"os"
	"testing"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/id/usermetadb/client"
	"github.com/google/go-cmp/cmp"
)

func TestServer_AddLog(t *testing.T) {
	defer os.Remove("test.db")
	srv, err := New(&Config{DBURI: "test.db"})
	if err != nil {
		t.Fatal("New", err)
	}

	httpSrv := httptest.NewServer(srv.Handler())
	defer httpSrv.Close()

	c, _ := client.New(&clientutil.BackendConfig{URL: httpSrv.URL})
	entries := generateTestLogs(100, generateAllRandomDevices())
	for _, e := range entries {
		if err := c.AddLog(context.Background(), "", e); err != nil {
			t.Fatalf("AddLog(%+v): %v", e, err)
		}
	}
}

func BenchmarkServer_AddLog(b *testing.B) {
	defer os.Remove("test.db")
	srv, err := New(&Config{DBURI: "test.db"})
	if err != nil {
		b.Fatal("New", err)
	}

	httpSrv := httptest.NewServer(srv.Handler())
	defer httpSrv.Close()

	b.ResetTimer()

	c, _ := client.New(&clientutil.BackendConfig{URL: httpSrv.URL})
	entries := generateTestLogs(b.N, generateAllRandomDevices())

	b.Run("AddLog", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			e := entries[i%len(entries)]
			if err := c.AddLog(context.Background(), "", e); err != nil {
				b.Fatalf("AddLog(%+v): %v", e, err)
			}
		}
	})
}

func TestServer_AddLastLogin(t *testing.T) {
	defer os.Remove("test.db")
	srv, err := New(&Config{DBURI: "test.db"})
	if err != nil {
		t.Fatal("New", err)
	}

	httpSrv := httptest.NewServer(srv.Handler())
	defer httpSrv.Close()

	c, _ := client.New(&clientutil.BackendConfig{URL: httpSrv.URL})
	entries := generateLastLoginEntries()
	for _, e := range entries {
		if err := c.SetLastLogin(context.Background(), "", e); err != nil {
			t.Fatalf("SetLastLogin(%+v): %v", e, err)
		}
	}

	expResp := *entries[0]
	expResp.Timestamp = roundTimestamp(expResp.Timestamp)
	resp, err := c.GetLastLogin(context.Background(), "", expResp.Username, expResp.Service)
	if err != nil {
		t.Fatal(err)
	}

	if diffs := cmp.Diff(resp[0], &expResp); diffs != "" {
		t.Fatalf("Last login entries differ:\n%s", diffs)
	}
}
