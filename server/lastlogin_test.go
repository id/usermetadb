package server

import (
	"context"
	"os"
	"sort"
	"testing"
	"time"

	"git.autistici.org/id/usermetadb"
	"github.com/google/go-cmp/cmp"
)

func generateLastLoginEntries() []*usermetadb.LastLoginEntry {
	timestamp := time.Now().UTC()
	entries := make([]*usermetadb.LastLoginEntry, 0)
	for _, u := range []string{"user1", "user2"} {
		for _, s := range []string{"service1", "service2"} {
			e := &usermetadb.LastLoginEntry{
				Timestamp: timestamp,
				Username:  u,
				Service:   s,
			}
			entries = append(entries, e)
		}
	}
	return entries
}

func BenchmarkLastLogin(b *testing.B) {
	defer os.Remove("bench.db")
	db, err := openDB("bench.db")
	if err != nil {
		b.Fatal(err)
	}
	defer db.Close()

	ll, err := newLastloginDB(db)
	if err != nil {
		b.Fatal(err)
	}
	defer ll.Close()

	entries := generateLastLoginEntries()

	b.Run("AddLastLogin", func(b *testing.B) {
		b.RunParallel(func(pb *testing.PB) {
			for i := 0; pb.Next(); i++ {
				// nolint: errcheck
				ll.AddLastLogin(context.Background(), entries[i%len(entries)])
			}
		})
	})
}

func TestLastlogin_LoginAdded(t *testing.T) {
	defer os.Remove("test.db")
	db, err := openDB("test.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	ll, err := newLastloginDB(db)
	if err != nil {
		t.Fatal(err)
	}
	defer ll.Close()

	in := generateLastLoginEntries()
	for _, e := range in {
		if err := ll.AddLastLogin(context.TODO(), e); err != nil {
			t.Fatal(err)
		}
	}

	out, err := ll.GetLastLogin(context.TODO(), in[0].Username, in[0].Service)
	if err != nil {
		t.Fatal(err)
	}

	if len(out) != 1 {
		t.Fatalf("Expected exactly one entry, found %v", out)
	}

	// Modify the entries so that the quantized timestamp matches.
	in[0].Timestamp = roundTimestamp(in[0].Timestamp)
	if diffs := cmp.Diff(in[0], out[0]); diffs != "" {
		t.Fatalf("Last login entries differ:\n%s", diffs)
	}
}

func TestLastLogin_MultipleServices(t *testing.T) {
	defer os.Remove("test.db")
	db, err := openDB("test.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	ll, err := newLastloginDB(db)
	if err != nil {
		t.Fatal(err)
	}
	defer ll.Close()

	in := generateLastLoginEntries()
	for _, e := range in {
		if err := ll.AddLastLogin(context.TODO(), e); err != nil {
			t.Fatal(err)
		}
	}

	out, err := ll.GetLastLogin(context.TODO(), in[0].Username, "")
	if err != nil {
		t.Fatal(err)
	}

	if len(out) != 2 {
		t.Fatalf("Expected exactly two entries, found %v", out)
	}

	for i := 0; i < 2; i++ {
		// Modify the entries so that the quantized timestamp matches.
		in[i].Timestamp = roundTimestamp(in[i].Timestamp)
		if diffs := cmp.Diff(in[0], out[0]); diffs != "" {
			t.Fatalf("Last login entries #%d differ:\n%s", i, diffs)
		}
	}
}

func TestLastLogin_NoUser(t *testing.T) {
	defer os.Remove("test.db")
	db, err := openDB("test.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	ll, err := newLastloginDB(db)
	if err != nil {
		t.Fatal(err)
	}
	defer ll.Close()

	in := generateLastLoginEntries()
	for _, e := range in {
		if err := ll.AddLastLogin(context.TODO(), e); err != nil {
			t.Fatal(err)
		}
	}

	out, err := ll.GetLastLogin(context.TODO(), "user3", "")
	if err != nil {
		t.Fatal(err)
	}

	if len(out) != 0 {
		t.Fatalf("Expected no entries, found %v", out)
	}
}

func TestLastLogin_UnusedAccounts(t *testing.T) {
	defer os.Remove("test.db")
	db, err := openDB("test.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	ll, err := newLastloginDB(db)
	if err != nil {
		t.Fatal(err)
	}
	defer ll.Close()

	entries := []*usermetadb.LastLoginEntry{
		&usermetadb.LastLoginEntry{
			Timestamp: time.Now().AddDate(0, 0, -30),
			Username:  "user1",
			Service:   "service1",
		},
		&usermetadb.LastLoginEntry{
			Timestamp: time.Now().AddDate(0, 0, -1),
			Username:  "user1",
			Service:   "service2",
		},
		&usermetadb.LastLoginEntry{
			Timestamp: time.Now().AddDate(0, 0, -30),
			Username:  "user2",
			Service:   "service1",
		},
	}
	for _, e := range entries {
		if err := ll.AddLastLogin(context.TODO(), e); err != nil {
			t.Fatal(err)
		}
	}

	// Now look for user1, user2 and user3.
	unused, err := ll.GetUnusedAccounts(context.TODO(), []string{"user1", "user2", "user3"}, 7)
	if err != nil {
		t.Fatalf("GetUnusedAccounts: %v", err)
	}
	sort.Strings(unused)

	if diffs := cmp.Diff([]string{"user2", "user3"}, unused); diffs != "" {
		t.Fatalf("GetUnusedAccounts returned unexpected result: %s", diffs)
	}
}
