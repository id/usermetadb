package server

import (
	"context"
	"database/sql"
	"log"
	"regexp"
	"time"

	"git.autistici.org/ai3/go-common/sqlutil"
	"git.autistici.org/id/usermetadb"
)

var (
	// How often to run the user log compaction.
	compactionInterval = 600 * time.Second

	// How often to run the log pruner.
	pruneInterval = 24 * time.Hour
)

var userlogDBStatements = map[string]string{
	// Modify the 'devices' table.
	"check_device_info": `SELECT 1 FROM devices WHERE username = ? AND id = ?`,
	"insert_device_info": `
            INSERT INTO devices (
                username, id,
		last_device_remote_zone, last_device_user_agent,
		device_browser, device_os, device_mobile,
                first_seen, last_seen
            ) VALUES (
                ?, ?, ?, ?, ?, ?, ?, ?, ?
            )`,
	"update_device_info": `
	    UPDATE devices SET
	        last_seen = ?, last_device_remote_zone = ?, last_device_user_agent = ?,
		device_browser = ?, device_os = ?, device_mobile = ?
	    WHERE username = ? AND id = ?
	`,

	// Insert records in the 'userlog' table (with or without
	// optional device information).
	"insert_userlog": `
	    INSERT INTO userlog (
	        username, service, log_type, login_method, login_authenticator_id, message, timestamp
            ) VALUES (
                ?, ?, ?, ?, ?, ?, ?
            )`,
	"insert_userlog_with_device_info": `
	    INSERT INTO userlog (
	        username, service, log_type, login_method, login_authenticator_id, message, timestamp,
		device_id, device_remote_zone, device_user_agent,
		device_browser, device_os, device_mobile
            ) VALUES (
                ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
            )`,

	// Database maintenance (prune old entries etc).
	"prune_userlog": `
	    DELETE FROM userlog WHERE timestamp < ?
	`,
	"prune_device_info": `
	    DELETE FROM devices WHERE last_seen < ?
	`,
	// Cut the log history for a specific user. This version is
	// sqlite-specific (uses "rowid"), when version 3.25 reaches
	// Debian stable we'll be able to switch to using a window
	// function (as in the commented version below).
	"consolidate_userlog": `
            DELETE FROM userlog WHERE rowid IN (
               SELECT rowid
               FROM userlog
               WHERE username = ?
               ORDER BY timestamp DESC LIMIT -1 OFFSET ?
            )
        `,
	// "consolidate_userlog": `
	//     WITH rows AS (
	//        SELECT rowid, ROW_NUMBER()
	//           OVER (PARTITION BY username ORDER BY timestamp DESC) n
	//        FROM userlog
	//        WHERE username = ? AND n > 100
	//     )
	//     DELETE FROM userlog WHERE rowid IN (SELECT n FROM rows)
	// `,

	// Retrieve logs for a specific user.
	"get_user_logs": `
	    SELECT
		service, log_type, login_method, login_authenticator_id, message, timestamp,
	        device_id, device_remote_zone, device_user_agent,
		device_browser, device_os, device_mobile
	    FROM
		userlog
	    WHERE
	        username = ? AND timestamp > ?
	    ORDER BY timestamp DESC
	    LIMIT ?
	`,

	// Summary of top known and recent devices for a user.
	"devices_with_counts": `
	    SELECT
		d.id, d.device_browser,
		d.device_os, d.device_mobile,
		d.last_device_user_agent, d.last_device_remote_zone,
		d.first_seen, d.last_seen,
		COUNT(*) AS count
	    FROM
		userlog AS u LEFT JOIN devices AS d ON u.device_id = d.id
	    WHERE
		u.username = ? AND u.log_type = 'login'
	    GROUP BY d.id
	    ORDER BY d.last_seen DESC
	    LIMIT 20
	`,
}

type userlogDB struct {
	db                *sql.DB
	cronjobs          []*cronJob
	pendingCompaction *usernameSet
	ignoredUsers      []*regexp.Regexp
}

func newUserlogDB(db *sql.DB, compactionKeepCount, maxAgeDays int, ignoredUsers []*regexp.Regexp) (*userlogDB, error) {
	udb := &userlogDB{
		db:           db,
		ignoredUsers: ignoredUsers,
	}

	// Selectively add cronjobs if the related parameters are non-zero.
	if compactionKeepCount > 0 {
		pc := newUsernameSet()
		udb.pendingCompaction = pc
		udb.cronjobs = append(udb.cronjobs, newCron(func() {
			pc.foreach(func(username string) {
				if err := compactLogs(db, username, compactionKeepCount); err != nil {
					log.Printf("error cleaning up logs for user %s: %v", username, err)
				}
			})
		}, compactionInterval))
	}
	if maxAgeDays > 0 {
		udb.cronjobs = append(udb.cronjobs, newCron(func() {
			if err := pruneLogs(db, maxAgeDays); err != nil {
				log.Printf("error in log pruning: %v", err)
			}
		}, pruneInterval))
	}

	return udb, nil
}

func (u *userlogDB) Close() {
	for _, j := range u.cronjobs {
		j.Stop()
	}
}

func (u *userlogDB) isIgnored(username string) bool {
	for _, rx := range u.ignoredUsers {
		if rx.MatchString(username) {
			return true
		}
	}
	return false
}

// Update or create an entry in the 'devices' table.
func (u *userlogDB) updateDeviceInfo(tx *sql.Tx, username string, deviceInfo *usermetadb.DeviceInfo) error {
	now := roundTimestamp(time.Now().UTC())

	var seen bool
	err := tx.QueryRow(userlogDBStatements["check_device_info"], username, deviceInfo.ID).Scan(&seen)
	if err == sql.ErrNoRows {
		_, err = tx.Exec(
			userlogDBStatements["insert_device_info"],
			username,
			deviceInfo.ID,
			deviceInfo.RemoteZone,
			deviceInfo.UserAgent,
			deviceInfo.Browser,
			deviceInfo.OS,
			deviceInfo.Mobile,
			now,
			now,
		)
	} else if err == nil {
		// We expect remote-zone and (maybe) user-agent to
		// change very frequently (hence the last_ prefix on
		// database columns), but still sometimes it can
		// legitimately happen that the other parameters
		// change too (like when restoring a backup to another
		// platform/os), so we update them too.
		_, err = tx.Exec(
			userlogDBStatements["update_device_info"],
			now,
			deviceInfo.RemoteZone,
			deviceInfo.UserAgent,
			deviceInfo.Browser,
			deviceInfo.OS,
			deviceInfo.Mobile,
			username,
			deviceInfo.ID,
		)
	}
	return err
}

func (u *userlogDB) AddLog(ctx context.Context, entry *usermetadb.LogEntry) error {
	// Check that the entry is well-formed.
	if err := entry.Validate(); err != nil {
		return err
	}

	minimizeLogEntry(entry)

	// Ignored users don't create log entries.
	if u.isIgnored(entry.Username) {
		return nil
	}

	// If we're given a DeviceInfo entry, update it or create it
	// if it does not exist yet.
	stmtName := "insert_userlog"
	args := []interface{}{
		entry.Username,
		entry.Service,
		entry.Type,
		entry.LoginMethod,
		entry.LoginAuthenticatorID,
		entry.Message,
		entry.Timestamp,
	}
	if entry.DeviceInfo != nil {
		stmtName = "insert_userlog_with_device_info"
		args = append(args, entry.DeviceInfo.ID)
		args = append(args, entry.DeviceInfo.RemoteZone)
		args = append(args, entry.DeviceInfo.UserAgent)
		args = append(args, entry.DeviceInfo.Browser)
		args = append(args, entry.DeviceInfo.OS)
		args = append(args, entry.DeviceInfo.Mobile)
	}

	if err := sqlutil.WithTx(ctx, u.db, func(tx *sql.Tx) error {
		if entry.DeviceInfo != nil {
			if err := u.updateDeviceInfo(tx, entry.Username, entry.DeviceInfo); err != nil {
				return err
			}
		}
		_, err := tx.Exec(userlogDBStatements[stmtName], args...)
		return err
	}); err != nil {
		return err
	}

	if entry.Username != "" && u.pendingCompaction != nil {
		u.pendingCompaction.add(entry.Username)
	}

	return nil
}

type nullBool bool

func (b *nullBool) Scan(value interface{}) error {
	if value != nil {
		if val, ok := value.(int64); ok && val == 1 {
			*b = nullBool(true)
		}
	}
	return nil
}

// Ah the joys of NULLs and database/sql.
func scanEntryRow(rows *sql.Rows) (*usermetadb.LogEntry, error) {
	var e usermetadb.LogEntry

	// Some fields might be NULL, but we want empty
	// strings instead (and also we'd like not to pollute
	// the LogEntry type with funny string wrappers).
	var deviceID, deviceRemoteZone, deviceUserAgent, deviceBrowser, deviceOS sql.NullString
	var deviceMobile nullBool
	if err := rows.Scan(
		&e.Service,
		&e.Type,
		&e.LoginMethod,
		&e.LoginAuthenticatorID,
		&e.Message,
		&e.Timestamp,
		&deviceID,
		&deviceRemoteZone,
		&deviceUserAgent,
		&deviceBrowser,
		&deviceOS,
		&deviceMobile,
	); err != nil {
		return nil, err
	}
	if deviceID.Valid {
		e.DeviceInfo = &usermetadb.DeviceInfo{
			ID:         deviceID.String,
			RemoteZone: deviceRemoteZone.String,
			UserAgent:  deviceUserAgent.String,
			Browser:    deviceBrowser.String,
			OS:         deviceOS.String,
			Mobile:     bool(deviceMobile),
		}
	}
	return &e, nil
}

func (u *userlogDB) GetUserLogs(ctx context.Context, username string, maxDays, limit int) ([]*usermetadb.LogEntry, error) {
	if maxDays == 0 {
		maxDays = 60
	}
	cutoff := time.Now().AddDate(0, 0, -maxDays).UTC()
	if limit == 0 {
		limit = 30
	}

	var out []*usermetadb.LogEntry
	err := sqlutil.WithReadonlyTx(ctx, u.db, func(tx *sql.Tx) error {
		rows, err := tx.Query(userlogDBStatements["get_user_logs"], username, cutoff, limit)
		if err != nil {
			return err
		}
		defer rows.Close()
		for rows.Next() {
			e, err := scanEntryRow(rows)
			if err != nil {
				return err
			}
			e.Username = username
			out = append(out, e)
		}
		return rows.Err()
	})
	return out, err
}

func (u *userlogDB) GetUserDevices(ctx context.Context, username string) ([]*usermetadb.MetaDeviceInfo, error) {
	var out []*usermetadb.MetaDeviceInfo
	err := sqlutil.WithReadonlyTx(ctx, u.db, func(tx *sql.Tx) error {
		rows, err := tx.Query(userlogDBStatements["devices_with_counts"], username)
		if err != nil {
			return err
		}
		defer rows.Close()
		for rows.Next() {
			mdi := &usermetadb.MetaDeviceInfo{DeviceInfo: &usermetadb.DeviceInfo{}}
			if err := rows.Scan(
				&mdi.DeviceInfo.ID,
				&mdi.DeviceInfo.Browser,
				&mdi.DeviceInfo.OS,
				&mdi.DeviceInfo.Mobile,
				&mdi.DeviceInfo.UserAgent,
				&mdi.DeviceInfo.RemoteZone,
				&mdi.FirstSeen,
				&mdi.LastSeen,
				&mdi.NumLogins,
			); err != nil {
				return err
			}
			out = append(out, mdi)
		}
		return rows.Err()
	})
	return out, err
}

const timestampRoundSeconds = 7200

// Quantize timestamps to minimize time correlation possibilities.
func roundTimestamp(t time.Time) time.Time {
	// Keep the original timezone information.
	return time.Unix((t.Unix()/timestampRoundSeconds)*timestampRoundSeconds, 0).In(t.Location())
}

// Minimize a log entry, removing all the data that we don't want to
// store (even if it's already missing in the database schema).
func minimizeLogEntry(e *usermetadb.LogEntry) {
	// Round timestamps to the nearest even hour.
	e.Timestamp = roundTimestamp(e.Timestamp)

	// Drop fields in DeviceInfo that we don't want.
	if e.DeviceInfo != nil {
		e.DeviceInfo.RemoteAddr = ""
	}
}
