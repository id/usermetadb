package server

import (
	"database/sql"
	"os"
	"testing"
)

func countLogs(db *sql.DB) int {
	var count int
	db.QueryRow("SELECT COUNT(*) FROM userlog").Scan(&count) // nolint
	return count
}

func TestPruneLogs(t *testing.T) {
	defer os.Remove("test.db")
	db, err := openDB("test.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	bulkLoadTestLogs(t, db)
	n := countLogs(db)
	if n == 0 {
		t.Fatal("no logs loaded?")
	}

	if err := pruneLogs(db, 365); err != nil {
		t.Fatal("pruneLogs():", err)
	}
	n2 := countLogs(db)
	if n2 >= n {
		t.Fatalf("unexpected log count after prune: %d (should be < %d)", n2, n)
	}
}

func countLogsForUser(db *sql.DB, username string) int {
	var count int
	db.QueryRow("SELECT COUNT(*) FROM userlog WHERE username = ?", username).Scan(&count) // nolint
	return count
}

func TestCompactLogs(t *testing.T) {
	defer os.Remove("test.db")
	db, err := openDB("test.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	bulkLoadTestLogs(t, db)
	user1 := randomUsernames[0]
	user2 := randomUsernames[1]
	n1 := countLogsForUser(db, user1)
	n2 := countLogsForUser(db, user2)
	if n1 == 0 {
		t.Fatal("no logs loaded?")
	}

	// Find a reasonable limit.
	limit := n1 / 2

	if err := compactLogs(db, user1, limit); err != nil {
		t.Fatal("compactLogs():", err)
	}

	n1post := countLogsForUser(db, user1)
	n2post := countLogsForUser(db, user2)
	if n2post != n2 {
		t.Errorf("log compaction removed logs for other users (%s: %d -> %d)", user2, n2, n2post)
	}
	if n1post == 0 {
		t.Error("log compaction wiped all user logs")
	}
	if n1post != limit {
		t.Errorf("unexpected log count after compaction: %d (should be %d)", n1post, limit)
	}
}
