package server

import (
	"database/sql"
	"log"
	"net/http"
	"regexp"

	"git.autistici.org/ai3/go-common/serverutil"

	"git.autistici.org/id/usermetadb"
)

// Config for the UserMetaServer.
type Config struct {
	DBURI               string   `yaml:"db_uri"`
	CompactionKeepCount int      `yaml:"compaction_keep_count"`
	MaxAgeDays          int      `yaml:"max_age_days"`
	IgnoredUsers        []string `yaml:"ignored_users"`
}

// UserMetaServer exposes the analysis service and the user metadata
// database over an HTTP API.
type UserMetaServer struct {
	db        *sql.DB
	config    *Config
	analysis  *analysisService
	userlog   *userlogDB
	lastlogin *lastloginDB
}

// New returns a new UserMetaServer with the given configuration.
func New(config *Config) (*UserMetaServer, error) {
	ignoredUsers, err := parseRegexps(config.IgnoredUsers)
	if err != nil {
		return nil, err
	}

	db, err := openDB(config.DBURI)
	if err != nil {
		return nil, err
	}

	analysis, err := newAnalysisService(db)
	if err != nil {
		db.Close() // nolint
		return nil, err
	}

	userlog, err := newUserlogDB(db, config.CompactionKeepCount, config.MaxAgeDays, ignoredUsers)
	if err != nil {
		db.Close() // nolint
		return nil, err
	}

	lastlogin, err := newLastloginDB(db)
	if err != nil {
		db.Close() // nolint
		return nil, err
	}

	return &UserMetaServer{
		db:        db,
		config:    config,
		analysis:  analysis,
		userlog:   userlog,
		lastlogin: lastlogin,
	}, nil
}

// Close the underlying database.
func (s *UserMetaServer) Close() {
	s.analysis.Close()
	s.userlog.Close()
	s.lastlogin.Close()
	s.db.Close() // nolint
}

func (s *UserMetaServer) handleCheckDevice(w http.ResponseWriter, r *http.Request) {
	var req usermetadb.CheckDeviceRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	seen, err := s.analysis.CheckDevice(r.Context(), req.Username, req.DeviceInfo)
	if err != nil {
		log.Printf("Store error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, &usermetadb.CheckDeviceResponse{Seen: seen})
}

func (s *UserMetaServer) handleAddLog(w http.ResponseWriter, r *http.Request) {
	var req usermetadb.AddLogRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	if err := s.userlog.AddLog(r.Context(), req.Log); err != nil {
		log.Printf("AddLog error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, &usermetadb.AddLogResponse{})
}

func (s *UserMetaServer) handleGetUserDevices(w http.ResponseWriter, r *http.Request) {
	var req usermetadb.GetUserDevicesRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	devs, err := s.userlog.GetUserDevices(r.Context(), req.Username)
	if err != nil {
		log.Printf("GetUserDevices error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, &usermetadb.GetUserDevicesResponse{Devices: devs})
}

func (s *UserMetaServer) handleGetUserLogs(w http.ResponseWriter, r *http.Request) {
	var req usermetadb.GetUserLogsRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	entries, err := s.userlog.GetUserLogs(r.Context(), req.Username, req.MaxDays, req.Limit)
	if err != nil {
		log.Printf("GetUserLogs error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, &usermetadb.GetUserLogsResponse{Results: entries})
}

func (s *UserMetaServer) handleSetLastLogin(w http.ResponseWriter, r *http.Request) {
	var req usermetadb.SetLastLoginRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	if err := s.lastlogin.AddLastLogin(r.Context(), req.LastLogin); err != nil {
		log.Printf("AddLastLogin error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, &usermetadb.SetLastLoginResponse{})
}

func (s *UserMetaServer) handleGetLastLogin(w http.ResponseWriter, r *http.Request) {
	var req usermetadb.GetLastLoginRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	entries, err := s.lastlogin.GetLastLogin(r.Context(), req.Username, req.Service)
	if err != nil {
		log.Printf("GetLastLogin error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, &usermetadb.GetLastLoginResponse{Results: entries})
}

func (s *UserMetaServer) handleGetUnusedAccounts(w http.ResponseWriter, r *http.Request) {
	var req usermetadb.GetUnusedAccountsRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	unused, err := s.lastlogin.GetUnusedAccounts(r.Context(), req.Usernames, req.Days)
	if err != nil {
		log.Printf("GetUnusedAccounts error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, &usermetadb.GetUnusedAccountsResponse{UnusedUsernames: unused})
}

// Handler returns a http.Handler for the HTTP API.
func (s *UserMetaServer) Handler() http.Handler {
	h := http.NewServeMux()
	h.HandleFunc("/api/check_device", s.handleCheckDevice)
	h.HandleFunc("/api/add_log", s.handleAddLog)
	h.HandleFunc("/api/get_user_devices", s.handleGetUserDevices)
	h.HandleFunc("/api/get_user_logs", s.handleGetUserLogs)
	h.HandleFunc("/api/set_last_login", s.handleSetLastLogin)
	h.HandleFunc("/api/get_last_login", s.handleGetLastLogin)
	h.HandleFunc("/api/get_unused_accounts", s.handleGetUnusedAccounts)
	return h
}

func parseRegexps(l []string) ([]*regexp.Regexp, error) {
	var out []*regexp.Regexp
	for _, pattern := range l {
		rx, err := regexp.Compile(pattern)
		if err != nil {
			return nil, err
		}
		out = append(out, rx)
	}
	return out, nil
}
