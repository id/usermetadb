package server

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"

	"git.autistici.org/ai3/go-common/sqlutil"
	"git.autistici.org/id/usermetadb"
)

var lastloginDBStatements = map[string]string{
	"insert_or_replace_last_login": `
		INSERT OR REPLACE INTO lastlogin (
			username, service, timestamp
		) VALUES (
			?, ?, ?
		)`,
	"get_service_last_login": `
		SELECT
			username, service, timestamp
		FROM
			lastlogin
		WHERE
			username = ? AND service = ?`,
	"get_last_login": `
		SELECT
			username, service, timestamp
		FROM
			lastlogin
		WHERE
			username = ?
		ORDER BY timestamp DESC`,
}

type lastloginDB struct {
	db *sql.DB
}

func newLastloginDB(db *sql.DB) (*lastloginDB, error) {
	return &lastloginDB{
		db: db,
	}, nil
}

func (l *lastloginDB) Close() {
}

func (l *lastloginDB) AddLastLogin(ctx context.Context, entry *usermetadb.LastLoginEntry) error {
	if entry == nil {
		return errors.New("received nil entry")
	}

	if err := entry.Validate(); err != nil {
		return err
	}

	args := []interface{}{
		entry.Username,
		entry.Service,
		roundTimestamp(entry.Timestamp),
	}

	return sqlutil.WithTx(ctx, l.db, func(tx *sql.Tx) error {
		_, err := tx.Exec(lastloginDBStatements["insert_or_replace_last_login"], args...)
		return err
	})
}

func (l *lastloginDB) GetLastLogin(ctx context.Context, username string, service string) ([]*usermetadb.LastLoginEntry, error) {
	var stmt string
	var args []interface{}
	if service != "" {
		stmt = "get_service_last_login"
		args = []interface{}{
			username,
			service,
		}
	} else {
		stmt = "get_last_login"
		args = []interface{}{
			username,
		}
	}

	var entries []*usermetadb.LastLoginEntry
	err := sqlutil.WithReadonlyTx(ctx, l.db, func(tx *sql.Tx) error {
		rows, err := tx.Query(lastloginDBStatements[stmt], args...)
		if err != nil {
			return err
		}
		defer rows.Close()

		for rows.Next() {
			e := &usermetadb.LastLoginEntry{}
			if err := rows.Scan(
				&e.Username,
				&e.Service,
				&e.Timestamp,
			); err != nil {
				return err
			}
			entries = append(entries, e)
		}

		return rows.Err()
	})
	return entries, err
}

func (l *lastloginDB) GetUnusedAccounts(ctx context.Context, usernames []string, days int) ([]string, error) {
	if len(usernames) < 1 {
		return nil, errors.New("no usernames given")
	}

	// To find users that have *never* logged in, we run the
	// reverse query in SQL (find users that have logged in within
	// the time frame), and return the set remainder.
	cutoff := time.Now().AddDate(0, 0, -days)
	q := fmt.Sprintf(
		"SELECT username FROM lastlogin WHERE username IN (?%s) AND timestamp > ?",
		strings.Repeat(",?", len(usernames)-1),
	)
	var args []interface{}
	usermap := make(map[string]struct{})
	for _, u := range usernames {
		args = append(args, u)
		usermap[u] = struct{}{}
	}
	args = append(args, cutoff)

	err := sqlutil.WithReadonlyTx(ctx, l.db, func(tx *sql.Tx) error {
		rows, err := tx.Query(q, args...)
		if err != nil {
			return err
		}
		defer rows.Close()
		for rows.Next() {
			var username string
			if err := rows.Scan(&username); err != nil {
				return err
			}
			delete(usermap, username)
		}
		return rows.Err()
	})
	if err != nil {
		return nil, err
	}

	var unused []string
	for username := range usermap {
		unused = append(unused, username)
	}
	return unused, nil
}
